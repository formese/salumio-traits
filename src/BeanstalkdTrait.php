<?php

namespace SalumIo\Traits;

use Beanstalk\Client;

trait BeanstalkdTrait
{
    /**
     * @var Client
     */
    protected $BeanstalkdClient;

    /**
     * @param Client $client Beanstalk Client
     */
    public function setBeanstalkd(Client $client)
    {
        $this->BeanstalkdClient = $client;
    }

    /**
     * @param bool $clone if client should be cloned or not. Defualt false
     * @return Client
     */
    public function getBeanstalkd($clone = false)
    {
        if ($clone) {
            return clone $this->BeanstalkdClient;
        }
        return $this->BeanstalkdClient;
    }
}
