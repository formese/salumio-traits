<?php

namespace SalumIo\Traits;

use MongoDB\Client;
use MongoDB\Collection;

trait MongoDbTrait
{
    /**
     * @var Client
     */
    protected $MongoClient;

    /**
     * @param Client $conn
     */
    public function setMongoClient(Client $conn)
    {
        $this->MongoClient = $conn;
    }

    /**
     * @param string $db
     * @return Collection
     */
    public function getMongoDb($db)
    {
        return $this->MongoClient->selectDatabase($db);
    }

    /**
     * @return Client
     */
    public function getMongoClient()
    {
        return $this->MongoClient;
    }
}
