<?php

namespace SalumIo\Traits;

use SalumIo\Crypto\Crypto;

trait CryptoTrait
{
    /**
     * @var Crypto
     */
    protected $crypto;

    /**
     * @param Crypto $crypto
     */
    public function setCryptoHandler(Crypto $crypto)
    {
        $this->crypto = $crypto;
    }

    /**
     * @return Crypto
     */
    public function getCryptoHandler()
    {
        return $this->crypto;
    }
}
