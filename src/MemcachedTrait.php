<?php

namespace SalumIo\Traits;

use Memcached;

trait MemcachedTrait
{
    /**
     * @var Memcached
     */
    protected $memcached;

    /**
     * @param Memcached $mem
     */
    public function setMemcached(Memcached $mem)
    {
        $this->memcached = $mem;
    }

    /**
     * @return Memcached
     */
    public function getMemcached()
    {
        return $this->memcached;
    }
}
