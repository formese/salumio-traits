<?php

namespace SalumIo\Traits;

use Aura\Session\Segment;
use Aura\Session\Session;

trait SessionTrait
{
    /**
     * @var Session
     */
    protected $ST_session;

    /**
     * @var Segment
     */
    protected $segment;

    /**
     * @param Session $session
     */
    public function setSession(Session $session)
    {
        $this->ST_session = $session;
        $this->segment = $session->getSegment(str_replace('\\', '/', get_class($this)));
    }

    /**
     * @param string|false $name
     * @param bool $raw
     * @return Segment|array
     */
    public function getSegment($name = false, $raw = false)
    {
        if (!$name) {
            return $this->segment;
        }
        if ($raw) {
            return isset($_SESSION[$name]) ? $_SESSION[$name] : [];
        }
        return $this->ST_session->getSegment($name);
    }
}
