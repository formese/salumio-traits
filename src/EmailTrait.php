<?php
namespace SalumIo\Traits;

use LitteraProcurator\EmailFactory;

trait EmailTrait
{
    protected $lpEmailFactory = false;

    public function setEmailFactory(EmailFactory $ef)
    {
        $this->lpEmailFactory = $ef;
    }

    public function createEmail()
    {
        return $this->lpEmailFactory->createEmail();
    }
}
