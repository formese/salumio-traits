<?php

namespace SalumIo\Traits;

use Blueprint\ITemplate as Blueprint;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response\HtmlResponse;

trait TemplateTrait
{
    /**
     * @param Blueprint $engine
     */
    public function setTemplateEngine(Blueprint $engine)
    {
        $this->template_engine = $engine;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return bool
     */
    public function assign($key, $value = null)
    {
        return $this->template_engine->assign($key, $value);
    }

    /**
     * @return Blueprint
     */
    public function getTemplateEngine()
    {
        return $this->template_engine;
    }

    /**
     * @param string $file
     * @return ResponseInterface
     */
    public function render($file = false)
    {
        $data = $this->template_engine->render($file);
        if (method_exists($this, 'getResponse')) {
            $response = $this->getResponse();
            $response->getBody()->write($data);
            return $response;
        }
        return new HtmlResponse($data);
    }
}
