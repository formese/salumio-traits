<?php
namespace SalumIo\Traits;

use SalumIo\Lutum\ModelFactory;

trait ModelTrait
{
    /**
     * @var mixed
     */
    protected $model = false;

    /**
     * @param ModelFactory $modelFactory
     */
    public function setModelFactory(ModelFactory $modelFactory)
    {
        $this->model = $modelFactory;
    }
}
