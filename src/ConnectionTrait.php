<?php

namespace SalumIo\Traits;

use Aura\Sql\ConnectionLocator;
use stdClass;

trait ConnectionTrait
{
    /**
     * @var PDO
     */
    protected $connection;

    /**
     * @var ConnectionLocator
     */
    protected $connection_locater;

    /**
     * @param string $type
     * @param string|null$name
     * @return PDO
     */
    public function getConnection($type = 'default', $name = null)
    {
        if ($this->connection_locater) {
            if ($type == 'write') {
                return $this->connection_locater->getWrite($name);
            } elseif ($type == 'read') {
                return $this->connection_locater->getRead($name);
            }

            return $this->connection_locater->getDefault();
        }
        return $this->connection ?: self::$connection;
    }

    /**
     * @param ConnectionLocator $locater
     */
    public function setConnectionLocator(ConnectionLocator $locater)
    {
        $this->connection_locater = $locater;
    }

    /**
     * @param PDO $conn
     */
    public function setConnection($conn)
    {
        $this->connection = $conn;
    }

    /**
     * @param string $sql
     * @return PDOStatement
     */
    public function prepareStmt($sql)
    {
        if ($this->connection_locater) {
            if (stripos($sql, 'select') === 0) {
                $connection = $this->getConnection('read');
            } else {
                $connection = $this->getConnection('write');
            }
            return $connection->prepare($sql);
        }
        if ($this->connection) {
            return $this->connection->prepare($sql);
        }
        return false;
    }

    /**
     * @param array $where
     * @return stdClass
     */
    public function _buildWhere(array $where)
    {
        $whereArr = [];
        $paramsArr = [];
        if (count($where)) {
            foreach ($where as $field => $info) {
                if (is_int($field) && is_array($info)) {
                    $whereArr[] = $info[0];
                    $params = (array) $info[1];
                    $keys = array_keys($params);

                    if (is_int($keys[0])) {
                        foreach ($params as $param) {
                            $paramsArr[] = $param;
                        }
                    } else {
                        $paramsArr += $params;
                    }
                } else {
                    if (is_int($field) && is_string($info)) {
                        $whereArr[] = $info;
                    } else {
                        $whereArr[] = $field . ' = :' . $field;
                        $paramsArr[$field] = $info;
                    }
                }
            }
        } else {
            $whereArr[] = '1=1';
        }
        $returnObj = new stdClass();
        $returnObj->where = implode(' AND ', $whereArr);
        $returnObj->params = $paramsArr;

        return $returnObj;
    }
}
