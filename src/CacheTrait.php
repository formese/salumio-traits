<?php

namespace SalumIo\Traits;

use Stash\Pool;

trait CacheTrait
{
    /**
     * @var Pool
     */
    protected $cache_pool;

    /**
     * @param Pool $pool
     */
    public function setCache(Pool $pool)
    {
        $this->cache_pool = $pool;
    }

    /**
     * @return Pool
     */
    public function getCachePool()
    {
        return $this->cache_pool;
    }

    /**
     * @param array $key
     * @return mixed
     */
    public function getCacheItem(array $key)
    {
        return $this->cache_pool->getItem(implode('/', $key));
    }
}
