<?php

namespace SalumIo\Traits;

use SalumIo\Cookie\Cookie;

trait CookieTrait
{
    /**
     * @var Cookie
     */
    protected $cookie;

    /**
     * @param Cookie $cookie
     */
    public function setCookieHandler(Cookie $cookie)
    {
        $this->cookie = $cookie;
    }

    /**
     * @return Cookie
     */
    public function getCookieHandler()
    {
        return $this->cookie;
    }
}
